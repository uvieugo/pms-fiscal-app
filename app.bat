@ECHO OFF
set app_dir=C:\fiscal_app
cd /D %app_dir%

set arg1=%1
set arg2=%2
set arg3=%3
set arg4=%4
set arg5=%5

echo %arg1% >> "C:\fiscal_app\app_startup.log"
echo %arg2% >> "C:\fiscal_app\app_startup.log"
echo %arg3% >> "C:\fiscal_app\app_startup.log"

node app\index.js %arg1% %arg2% %arg3%

EXIT