const makeResponse = (command,message) => {
  let return_code
  let status_code
  let fiscal_folio_number 
  if(message==false){
    return_code = "Message not sent"
    status_code = 'OK'
    fiscal_folio_number = 'null'
  }else{
    return_code = message.hasOwnProperty('errno') ? 'Fiscal IFC Down' : message.returnCodeLink
    status_code = message.hasOwnProperty('errno') ? 'error' : 'SUCCESS'
    fiscal_folio_number = message.hasOwnProperty('errno') ? 'null' : message.FiscalFolioNo
  }
  let final_result = `
  <?xml version="1.0" ?>
  <command>${command}</command>
  <head>
  <message_status>${status_code}</message_status>
  <message>
    ${return_code}
  </message>
  </head>
  <body>
  <fiscal_folio_number>${fiscal_folio_number}</fiscal_folio_number>
  </body>
  `
  return final_result
}

exports.makeResponse = makeResponse