const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  middleware_url: process.env.MIDDLEWARE_URL,
  tax_1_percent: process.env.TAX_1_PERCENT,
  tax_2_percent: process.env.TAX_2_PERCENT,
  tax_3_percent: process.env.TAX_3_PERCENT,
  log_file_name: process.env.LOG_FILE_NAME
};