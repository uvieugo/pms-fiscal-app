const fs = require('fs')
const {log_file_name, tax_1_percent, tax_2_percent, tax_3_percent} = require('./config');

const parseXml = (xml_data) => {

  let headData = xml_data.children.find(x => x.name == 'head')
  let bodyData = xml_data.children.find(x => x.name == 'body')
  let documentInfo = headData.children.find(x => x.name == 'document_info')
  let generalInfo = headData.children.find(x => x.name == 'general')
  let roomNumber = generalInfo.children.find(x => x.name == 'ROOMNUMBER').value
  let final_result
  let bill_number = documentInfo.children.find(x => x.name == 'BILLNUMBER').value
  let total_info = bodyData.children.find(x => x.name == 'total_info')
  let payment_info = bodyData.children.find(x => x.name == 'payment')
  let total_value = total_info.children.find(x => x.name == "TOTALGROSSAMOUNT")
  let base_value = total_info.children.find(x => x.name == "TOTALNETAMOUNT")
  let tax1 = total_info.children.find(x => x.name == "TAX1AMOUNT")
  let tax2 = total_info.children.find(x => x.name == "TAX2AMOUNT")
  let tax3 = total_info.children.find(x => x.name == "TAX3AMOUNT")

  console.log(payment_info)
  if (payment_info == undefined){
    final_result = false
  }else{
    final_result = {
      bill_datetime: new Date().toISOString().slice(0,-5),
      bill_number: bill_number,
      total_value: total_value.value,
      net_value: base_value.value,
      tax: [
        {
          Name: "1",
          Value: tax1.value,
          Percent: tax_1_percent
        },
        {
          Name: "2",
          Value: tax2.value,
          Percent: tax_2_percent
        },
        {
          Name: "3",
          Value: tax3.value,
          Percent: tax_3_percent
        }
      ]
    }
  
    if (roomNumber != ""){
      final_result.room_number = roomNumber
    }
  }


  fs.appendFile(log_file_name, JSON.stringify(final_result)+"\n", err => {})
  return final_result
}

exports.parseXml = parseXml