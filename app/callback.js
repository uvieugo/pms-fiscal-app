const fs = require('fs')
const needle = require('needle')
const {log_file_name} = require('./config');

const getCallback = (xmlResponse,jsonResponse,server_params,callback_url) => {

  let options = {
    rejectUnauthorized : false,
    headers: { 
      'Content-Type': 'text/xml',
    }
  }
  let return_code
  let status_code
  let fiscal_folio_number 
  if(jsonResponse==false){
    return_code = "Message not sent"
    status_code = 'OK'
    fiscal_folio_number = 'null'
  }else{
    return_code = jsonResponse.hasOwnProperty('errno') ? 'Fiscal IFC Down' : jsonResponse.returnCodeLink
    status_code = jsonResponse.hasOwnProperty('errno') ? 'error' : 'SUCCESS'
    fiscal_folio_number = jsonResponse.hasOwnProperty('errno') ? 'null' : jsonResponse.FiscalFolioNo
  }
  // let return_code = jsonResponse.hasOwnProperty('errno') ? 'Fiscal IFC Down' : jsonResponse.returnCode
  // let status_code = jsonResponse.hasOwnProperty('errno') ? 'error' : 'SUCCESS'
  // let fiscal_folio_number = jsonResponse.hasOwnProperty('errno') ? 'null' : jsonResponse.FiscalFolioNo
  let new_params

  new_params = server_params.split(/@/g).map( xy  => {
    if(xy.match('in_mesg_status')){
      return `in_mesg_status=!${status_code}!`
    }else if( xy.match('in_mesg_text') ){
      return `in_mesg_text=!${return_code}!`
    }else if( xy.match('in_fiscal_bill_no') ){
      return `in_fiscal_bill_no=!${fiscal_folio_number}!`
    }
    else{
      return xy
    }
  })

  // console.log(new_params.join("@"))
  fs.appendFile(log_file_name, `Posted below to pms \n`, err => {})
  fs.appendFile(log_file_name, `${callback_url}?${new_params.join("@")} \n`, err => {})

  needle.post(`${callback_url}?${new_params.join("@")}`, "", options, (err, resp) => {
    if (err) {
      // console.log(err)
    }else{
      // console.log(resp.body)
    }
  });

}

exports.getCallback = getCallback