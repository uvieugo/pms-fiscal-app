const needle = require('needle')
var options = {
  rejectUnauthorized : false
}

const getPayload = (payload_url) => {
  return new Promise(resolve => {
    needle.get(payload_url, options, (err, resp) => {
      if(err){
        console.log(err)
      }else{
        resolve(resp.body)
      }
    });

  });

}

exports.getPayload = getPayload