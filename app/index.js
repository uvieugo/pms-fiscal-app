const fs = require('fs')
const {getPayload} = require('./get_payload')
const {parseXml} = require('./parse_xml')
const {postToMiddleware} = require('./post_to_mid')
const {getCallback} = require('./callback')
const {makeResponse} = require('./response')
const {log_file_name} = require('./config')
let args = process.argv

let payload_url = args[2]
let server_params = args[3]
let callback_url = args[4]

const startApp = async () => {
  fs.appendFile(log_file_name, '********BEGIN FISCAL********* \n', err => {})
  fs.appendFile(log_file_name, payload_url+'\n', err => {})
  fs.appendFile(log_file_name, server_params+'\n', err => {})
  fs.appendFile(log_file_name, callback_url+'\n', err => {})

  fs.appendFile(log_file_name, '********GET PAYLOAD BEGIN******** \n', err => {})
  let xml_data = await getPayload(payload_url)
  fs.appendFile(log_file_name, JSON.stringify(xml_data), err => {})
  fs.appendFile(log_file_name, '\n', err => {})
  fs.appendFile(log_file_name, '********GET PAYLOAD END******** \n', err => {})

  fs.appendFile(log_file_name, '********PARSE PAYLOAD BEGIN******** \n', err => {})
  let post_data = parseXml(xml_data)
  fs.appendFile(log_file_name, '********PARSE PAYLOAD END********** \n', err => {})

  let command = xml_data.children[0].value
  // POST to middleware
  let response
  fs.appendFile(log_file_name, '******** POST MIDDLEWARE BEGIN ******** \n', err => {})
  if(post_data==false){
    response=false
  }else{
    response = await postToMiddleware(post_data)
  }
  fs.appendFile(log_file_name, '********* POST MIDDLEWARE END ********* \n', err => {})

  let xmlResponse = makeResponse(command, response)
  console.log(xmlResponse)
  fs.appendFile(log_file_name, '********POST RESPONSE TO PMS******** \n', err => {})
  getCallback(xmlResponse,response,server_params,callback_url)
  fs.appendFile(log_file_name, '********POST RESPONSE TO PMS END******** \n', err => {})

  fs.appendFile(log_file_name, '********END FISCAL********* \n', err => {})
}

startApp()
